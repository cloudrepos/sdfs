/*
 * DataBuffer.cpp
 *
 *  Created on: 2013年8月17日
 *      Author: sun
 */

#include "DataBuffer.h"
#include <errno.h>

namespace sdfs {

DataBuffer::DataBuffer(int size) {
	m_size = size;
	m_buf = new char[m_size];
	p_input = m_buf;
	p_output = m_buf;
	p_end = m_buf + m_size;
}

DataBuffer::~DataBuffer() {
	delete m_buf;
	m_buf = NULL;
}

int DataBuffer::Write(char *buf, int size)
{
	if(p_output+size > p_end)
		return ENOMEM;
	//TODO
	return 0;
}

} /* namespace sdfs */
