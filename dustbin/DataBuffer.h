/*
 * DataBuffer.h
 *
 *  Created on: 2013年8月17日
 *      Author: sun
 */

#ifndef DATABUFFER_H_
#define DATABUFFER_H_

namespace sdfs {

class DataBuffer {
public:
	DataBuffer(int size);
	virtual ~DataBuffer();

	int Write(char *buf, int size);

private:
	char *m_buf;
	char *p_input;
	char *p_output;
	char *p_end;
	int		m_size;
};

} /* namespace sdfs */
#endif /* DATABUFFER_H_ */
