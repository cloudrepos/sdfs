/*
 * TrackerWorker.h
 *
 *  Created on: 2013年7月29日
 *      Author: sun
 */

#ifndef TRACKERWORKER_H_
#define TRACKERWORKER_H_

#include "Framework.h"
#include "IRunnable.h"
#include "ITaskStrategy.h"

namespace sdfs {

class TrackerWorker: public IRunnable, public ITaskStrategy {
public:
	TrackerWorker();
	virtual ~TrackerWorker();

	void *Run(void *arg);

	void Stop();

	ITaskCarrier* GetCarrier(void *header);
private:

	vector<ITaskCarrier*> carriers;
};
} /* namespace sdfs */
#endif /* TRACKERWORKER_H_ */
