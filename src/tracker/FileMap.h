/*
 * FileMap.h
 *
 *  Created on: 2013年8月21日
 *      Author: sun
 */

#ifndef FILEMAP_H_
#define FILEMAP_H_
#include "tracker_global.h"
#include "ConnectionPool.h"
#include "RedisAdaptor.h"
#include "RedisReply.h"

namespace sdfs {

class FileMap {
public:
	FileMap(int connection_size);

	virtual ~FileMap();

	void GetFileRecord(RedisReply &reply, const char *hashcode);

	struct fileInfo* GetFileInfo(RedisReply &reply);

	struct blockInfo* GetBlockInfoAt(RedisReply &reply, int idx);

	size_t	GetBlockCount(RedisReply &reply);

	int AddFileInfo(struct fileInfo *info, int size);

	int RemoveFileInfo(const char *hashcode);

	int AddBlockInfo(const char *hashcode, struct blockInfo *info, int size);

	void Close();

	static FileMap *GetInstance();

private:
	ConnectionPool *m_db_pool;
	int				m_size;
	static const FileMap *instance;
};

} /* namespace sdfs */
#endif /* FILEMAP_H_ */
