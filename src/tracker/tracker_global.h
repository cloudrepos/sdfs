#ifndef _TRACKER_GLOBAL_H_
#define _TRACKER_GLOBAL_H_

#include "Framework.h"
#include "ITaskCarrier.h"
#include "SearchCarrier.h"

namespace sdfs{

#define STORAGE_FILE_BASE "/home/sun/data/M00/00/"

struct fileInfo
{
	char filename[256];
	char hashcode[266];//256 hash code by SHA-256
						//10 size number
	long long size;
	struct timeval timeval;
};

struct blockInfo
{
	char hashcode[266];
	long	size;
	char storagename[16];
};

extern SearchCarrier g_Search_Carrier;

extern ServerConfig *p_db_config;
extern ServerConfig *p_tracker_config;
extern ServerConfig *p_storage_configs;
extern size_t storage_size;
extern int tracker_port;
}

#endif
