/*
 * Searcher.h
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#ifndef SEARCHER_H_
#define SEARCHER_H_

#include "Framework.h"
#include "SDFSResponse.h"
#include "SDFSRequest.h"
#include "ITaskCarrier.h"

namespace sdfs {

class SearchCarrier : public ITaskCarrier{
public:
	SearchCarrier();
	virtual ~SearchCarrier();

	virtual bool isTasktype(void* head);

	virtual void* Work(Request *req, Response *resp);

	void SendFileInfo(SDFSResponse* resp,
			unsigned char ver, int status,
			long long size,
			const char *hashcode);

private:
	req_method_t method;
	unsigned char version;
};

} /* namespace sdfs */
#endif /* SEARCHER_H_ */
