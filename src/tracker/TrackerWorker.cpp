/*
 * TrackerWorker.cpp
 *
 *  Created on: 2013年7月29日
 *      Author: sun
 */

#include "TrackerWorker.h"
#include "common_define.h"
#include "Log.h"
#include "Socket.h"
#include "TrackerConnectionPool.h"
#include "SearchCarrier.h"
#include "tracker_global.h"

namespace sdfs {

SearchCarrier g_Search_Carrier;

TrackerWorker::TrackerWorker() {
	carriers.push_back(&g_Search_Carrier);
}

TrackerWorker::~TrackerWorker() {
}

void *TrackerWorker::Run(void *arg)
{
	//struct Share_Data *pData = (struct Share_Data *)arg;
	Log::Debug("TrackerWorker begin woring...");
	Task* task = (Task *)arg;
	SDFSRequest req(task);
	SDFSResponse resp(task);
	int err = req.ReadHeader();
	if(err != 0)
	{
		return (void *)err;
	}
	ITaskCarrier *carrier = GetCarrier(req.GetRequestHead());
	if(carrier == NULL)
		return (void *)ENOCMD;
	err = (int)carrier->Work(&req, &resp);
	Log::Debug("task->Destroy();");
	task->Destroy();
	return (void *)err;
};

void TrackerWorker::Stop()
{
	Log::Debug("TestRunner Stop");
}

ITaskCarrier* TrackerWorker::GetCarrier(void *header)
{
	RequsetHeader *rhead = (RequsetHeader *)header;
	vector<ITaskCarrier *>::iterator it = carriers.begin();
	for( ; it != carriers.end() ; ++it)
	{
		if((*it)->isTasktype(rhead))
			return *it;
	}
	return NULL;
}


} /* namespace sdfs */
