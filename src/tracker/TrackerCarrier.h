/*
 * UploadCarrier.h
 *
 *  Created on: 2013年8月22日
 *      Author: sun
 */
#ifndef UPLOADCARRIER_H_
#define UPLOADCARRIER_H_
#include "ITaskCarrier.h"

namespace sdfs {

	class TrackerCarrier:public ITaskCarrier {
	public:
		TrackerCarrier();
		virtual ~TrackerCarrier();

		bool isTasktype(void* head);
		/**
		 * Tracker recieve upload request from client
		 * ******************************************
		 * Format:
		 * * foriegn request based on HTTP
		 * PUT /filename HTTP/1.0
		 *
		 * * response based on HTTP
		 * -----------------SUCCESS-------------
		 * HTTP/1.0 200 OK
		 * Content-type: text/plain
		 * Content-length: 6
		 *
		 *
		 * SUCCESS
		 * -----------------END----------------
		 * ----------------ERROR---------------
		 * HTTP/1.0 50x ERRORINFO
		 * Content-type: text/plain
		 * Content-length: 5
		 *
		 *
		 * ERROR
		 * -----------------END---------------
		 * ******************************************
		 */
		virtual void* Upload(Request *req, Response *resp);
		virtual void* Delete(Request *req, Response *resp);
		virtual void* Download(Request *req, Response *resp);
		virtual void* Get(Request *req, Response *resp);

	private:
		ver_t m_ver;
		req_method_t m_method;
	};

} /* namespace sdfs */
#endif /* UPLOADCARRIER_H_ */           
