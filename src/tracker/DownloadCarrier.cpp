/*
 * DownloadCarrier.cpp
 *
 *  Created on: 2013年8月22日
 *      Author: sun
 */

#include "DownloadCarrier.h"
#include "Framework.h"
#include "SDFSRequest.h"

namespace sdfs {

DownloadCarrier::DownloadCarrier() {
	m_version = 0x01;
	m_method = _download_;
}

DownloadCarrier::~DownloadCarrier() {
}

bool DownloadCarrier::isTasktype(void *head)
{
	SDFSRequest *request = (SDFSRequest *)head;
	return request->GetVersion() == m_version && request->GetMethod() == m_method;
}

void *DownloadCarrier::Work(Request *req, Response *resp)
{

}

} /* namespace sdfs */
