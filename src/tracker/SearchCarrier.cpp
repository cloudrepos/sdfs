/*
 * Searcher.cpp
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#include "SearchCarrier.h"
#include "common_define.h"
#include "Log.h"
#include "TrackerConnectionPool.h"
#include "FileMap.h"
#include "CToolKit.h"
#include "tracker_global.h"

namespace sdfs {

SearchCarrier::SearchCarrier() {
	method = _search_;
	version = 0x01;
}

SearchCarrier::~SearchCarrier() {
}

bool SearchCarrier::isTasktype(void* _head)
{
	RequsetHeader *head = (RequsetHeader *)_head;
	//TODO test mode
	//return _search_ == head->method && version == head->ver ? true : false;
	return true;
}

void SearchCarrier::SendFileInfo(SDFSResponse* resp,
		unsigned char ver, int status,
		long long size,
		const char *hashcode)
{
	resp->SetVer(ver);
	resp->SetStatusCode(RESP_STATUS_OK);
	resp->SetContextLength(size);
	resp->SetHashCode(hashcode);

	resp->SendHeader();
}

void *SearchCarrier::Work(Request *req, Response *resp)
{
	char buf[1024];
	struct ResponseHeader head;
	char hashcode[266];
	struct fileInfo* file;
	SDFSRequest *request = (SDFSRequest*)req;
	SDFSResponse *response = (SDFSResponse*)resp;
	size_t size = request->ReadContext(hashcode, 266);
	//search dispatched storages from local1 database by name
	FileMap *map = FileMap::GetInstance();
	RedisReply reply;
	Log::Debug("map->GetFileRecord(reply, hashcode);");
	map->GetFileRecord(reply, hashcode);
	Log::Debug("file = map->GetFileInfo(reply);");
	file = map->GetFileInfo(reply);
	Log::Debug("SendFileInfo");
	SendFileInfo(response, version, RESP_STATUS_OK, file->size, file->hashcode);

	return (void *)0;
}


} /* namespace sdfs */
