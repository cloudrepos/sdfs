/*
 * Tracker.h
 *
 *  Created on: 2013年7月27日
 *      Author: sun
 */

#ifndef TRACKER_H_
#define TRACKER_H_

#include "TrackerService.h"
#include "common_define.h"
#include "Epoll.h"
#include "IRunnable.h"
#include "Socket.h"
#include "ConnectionPool.h"
#include "Thread.h"
#include "ThreadPool.h"
#include "TrackerConnectionPool.h"
#include "FileMap.h"

namespace sdfs {

typedef struct{
	ServerConfig	db;
	ServerConfig	tracker;
	ServerConfig	*backend;
	int				work_thread_size;
	int				db_conn_size;
	//TODO: other configurations...
}TrackerConfig;

class Tracker: public TrackerService {
public:
	Tracker(IRunnable *runner);
	virtual ~Tracker();
protected:
	int ReadConfiguration();

	int OpenListenSocket();

	int ConnectBackendServer();

	int InitFileTable();

	int InitConnectionPool();

	int StartWorkers();

	int CloseConnectionPool();

	int StopWorkers();

	int CloseFileTable();

	int CloseListenSocket();
public:
	int AddTask(int fd);
private:
	TrackerConfig	m_sConf;
	Socket			*m_tracker;
	TrackerConnectionPool *m_backed_conn;
	IRunnable		*m_runner;
	Thread			*m_thread_threadpool;
	ThreadPool		*m_threadpool;
	FileMap			*m_files;
};

} /* namespace sdfs */
#endif /* TRACKER_H_ */
