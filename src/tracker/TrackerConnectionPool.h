/*
 * TrackerConnectionPool.h
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#ifndef TRACKERCONNECTIONPOOL_H_
#define TRACKERCONNECTIONPOOL_H_

#include "../framework/ConnectionPool.h"
#include "../common/common_define.h"
#include "../framework/Socket.h"

using namespace std;

namespace sdfs {

struct fdpair
{
	char name[SRV_NAME_LEN];
	int index;
};

class TrackerConnectionPool{

public:
	TrackerConnectionPool(ServerConfig *confs,
			int serverSize, int poolsize, bool isBlock = true);
	virtual ~TrackerConnectionPool();

	//static TrackerConnectionPool *GetInstance(int serverSize, const ServerConfig *confs);

	Socket *GetConnection(const char* name);

	int ReturnConnection(Socket *socket);

	static TrackerConnectionPool* GetInstance();
private:
	int GetIndex(const char* name);

private:
	//static TrackerConnectionPool *instance;
	ConnectionPool	**m_socket_pool;
	fdpair			*m_registetable;
	int 			m_nPoolsize;
	int				m_bBlock;
	int				m_nSize;
	static const TrackerConnectionPool *instance;
};

} /* namespace sdfs */
#endif /* TRACKERCONNECTIONPOOL_H_ */
