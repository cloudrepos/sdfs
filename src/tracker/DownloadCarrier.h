/*
 * DownloadCarrier.h
 *
 *  Created on: 2013年8月22日
 *      Author: sun
 */

#ifndef DOWNLOADCARRIER_H_
#define DOWNLOADCARRIER_H_

#include "ITaskCarrier.h"

namespace sdfs {

class DownloadCarrier : public ITaskCarrier{
public:
	DownloadCarrier();
	virtual ~DownloadCarrier();

	bool isTasktype(void* head);

	void *Work(Request* req, Response* resp);
private:
	ver_t m_version;
	req_method_t m_method;
};

} /* namespace sdfs */
#endif /* DOWNLOADCARRIER_H_ */
