/*
 * Tracker.cpp
 *
 *  Created on: 2013年7月27日
 *      Author: sun
 */

#include "Tracker.h"
#include "common_define.h"
#include "CToolKit.h"
#include "Log.h"
#include "tracker_global.h"

namespace sdfs {

ServerConfig *p_db_config;
ServerConfig *p_tracker_config;
ServerConfig *p_storage_configs;
size_t		  storage_size;
int			  tracker_port;

Tracker::Tracker(IRunnable *runner) {
	m_runner = runner;
	m_tracker = NULL;
	m_backed_conn = NULL;
	m_threadpool = NULL;
	m_thread_threadpool = NULL;
	m_sConf.backend = NULL;
	m_files = NULL;
}

Tracker::~Tracker() {
	Log::Debug("~Tracker");
	if(m_tracker != NULL)
		delete m_tracker;
	m_tracker = NULL;
	Log::Debug("delete m_backed_conn");
	if(m_backed_conn != NULL)
		delete m_backed_conn;
	Log::Debug("delete m_threadpool");
	if(m_threadpool != NULL)
		delete m_threadpool;
	m_threadpool = NULL;
	Log::Debug("delete m_thread_threadpool");
	if(m_thread_threadpool != NULL)
		delete m_thread_threadpool;
	m_thread_threadpool = NULL;
	if(m_sConf.backend != NULL)
		delete m_sConf.backend;
	m_sConf.backend = NULL;
}

int Tracker::ReadConfiguration()
{
	Log::Debug("ReadConfiguration");
	//TODO: read from configuration file

	//TEST
	//init tracker
	tracker_port = 8500;
	storage_size = 1;

	m_sConf.work_thread_size = 4;
	m_sConf.db_conn_size = 10;

	strcpy(m_sConf.db.strIp, LOCAL_HOST);
	strcpy(m_sConf.db.strName, "Redis01");
	m_sConf.db.nPort = 6379;
	p_db_config = &(m_sConf.db);

	strcpy(m_sConf.tracker.strIp, LOCAL_HOST);
	strcpy(m_sConf.tracker.strName, "Tracker01");
	m_sConf.tracker.nPort	= tracker_port;
	CToolKit::GetIpByName(m_sConf.tracker.strName, m_sConf.tracker.strIp, IP4_STR_LEN);
	p_tracker_config = &(m_sConf.tracker);

	m_tracker = new Socket(&(m_sConf.tracker));
	m_sConf.backend = new ServerConfig[storage_size];
	for(int i = 0 ;i < storage_size; i++)
	{
		snprintf(m_sConf.backend[i].strName, SRV_NAME_LEN, "storage0%d", 1+i);
		m_sConf.backend[i].nPort = 8505;
		CToolKit::GetIpByName(m_sConf.backend[i].strName, m_sConf.backend[i].strIp, IP4_STR_LEN);
	}
	p_storage_configs = m_sConf.backend;
	return 0;
}

int Tracker::OpenListenSocket()
{
	Log::Debug("OpenListenSocket");
	int result = 0;
	if((result = m_tracker->CreateListenSocket())>0)
		Log::Info("Successfull open listener file descriptor");
	return result;
}

int Tracker::InitFileTable()
{
	Log::Debug("InitFileTable");
	m_files = new FileMap(m_sConf.db_conn_size);
	return 0;
}

int Tracker::InitConnectionPool()
{
	Log::Debug("InitConnectionPool");
	m_backed_conn = new TrackerConnectionPool(m_sConf.backend, BACKED_SERVER_SIZE,
			CONNECTION_POOL_SIZE,true);
	return 0;
}

int Tracker::StartWorkers()
{
	Log::Debug("StartWorkers");
	m_threadpool = new ThreadPool(*m_runner);
	m_thread_threadpool = new Thread(m_threadpool);
	return m_thread_threadpool->Start();
}

int Tracker::CloseConnectionPool()
{
	Log::Debug("CloseConnectionPool");
	return 0;
}

int Tracker::StopWorkers()
{
	Log::Debug("StopWorkers");
	m_thread_threadpool->Stop();
	return 0;
}

int Tracker::CloseFileTable()
{
	Log::Debug("CloseFileTable");
	m_files->Close();
	return 0;
}

int Tracker::CloseListenSocket()
{
	Log::Debug("CloseListenSocket");
	return m_tracker->DisConnect();
}

int Tracker::AddTask(int fd)
{
	return m_threadpool->addTask(fd);
}

} /* namespace sdfs */
