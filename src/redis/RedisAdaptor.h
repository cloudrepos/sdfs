/*
 * RedisHelper.h
 *
 *  Created on: 2013年8月19日
 *      Author: sun
 */

#ifndef REDISHELPER_H_
#define REDISHELPER_H_

#include <hiredis/hiredis.h>
#include "RedisReply.h"
#include "IConnector.h"
#include "Framework.h"

namespace sdfs {

class RedisAdaptor:public IConnector {
public:
	RedisAdaptor(ServerConfig *config);
	virtual ~RedisAdaptor();

	int Connect();

	int DisConnect();

	RedisAdaptor *Clone();

	void Command(RedisReply &reply,const char *cmd, ...);
private:
	redisContext *m_rc;
	ServerConfig *m_config;
};

} /* namespace sdfs */
#endif /* REDISHELPER_H_ */
