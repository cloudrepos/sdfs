/*
 * RedisReply.cpp
 *
 *  Created on: 2013年8月20日
 *      Author: sun
 */

#include "RedisReply.h"
#include <hiredis/hiredis.h>

namespace sdfs {

RedisReply::RedisReply() {
	m_reply = NULL;
}

RedisReply::~RedisReply() {
	if(m_reply != NULL)
		freeReplyObject(m_reply);
	m_reply = NULL;
}

RedisReply& RedisReply::operator=(redisReply *reply)
{
	if(m_reply != NULL)
	{
		freeReplyObject(m_reply);
	}
	m_reply = reply;
	return *this;
}

char *RedisReply::Str()
{
	if(m_reply)
		return m_reply->str;
	return NULL;
}

int RedisReply::Str_len()
{
	if(m_reply)
		return m_reply->len;
	return -1;
}

long long RedisReply::Integer()
{
	if(m_reply)
		return m_reply->integer;
	return -1L;
}

int RedisReply::Type()
{
	if(m_reply)
		return m_reply->type;
	return -1;
}

size_t RedisReply::Array_size()
{
	if(m_reply)
		return m_reply->elements;
	return -1;
}

redisReply *RedisReply::Array_at(int index)
{
	if(m_reply)
		return m_reply->element[index];
	return NULL;
}

} /* namespace sdfs */
