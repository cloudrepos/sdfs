/*
 * RedisReply.h
 *
 *  Created on: 2013年8月20日
 *      Author: sun
 */

#ifndef REDISREPLY_H_
#define REDISREPLY_H_

#include <hiredis/hiredis.h>

namespace sdfs {

class RedisReply {
public:
	RedisReply();
	virtual ~RedisReply();

	RedisReply& operator = (redisReply *reply);

	char *Str();
	int Str_len();
	long long Integer();
	int Type();
	size_t Array_size();
	redisReply *Array_at(int index);

private:
	redisReply *m_reply;
};

} /* namespace sdfs */
#endif /* REDISREPLY_H_ */
