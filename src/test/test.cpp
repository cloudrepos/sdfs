#include <iostream>
#include <string.h>
#include <sys/time.h>
#include "RedisAdaptor.h"
#include "RedisReply.h"

using namespace std;
using namespace sdfs;

struct fileInfo
{
	char filename[256];
	char hashcode[266];
	long long size;
	struct timeval timeval;
};

struct blockInfo
{
	char hashcode[266];
	long	size;
	char storagename[16];
};

int main22()
{
	struct fileInfo file;
	struct fileInfo *p_file;
	struct blockInfo block;
	struct blockInfo *p_block;
	const char *file_hashcode = "61a7c976a2b0590725322c7284126eb0daf8a5d8ad6265cbd545e6a9338f17390083886080";
	const char *block_hashcode = "4b2f4d66638f1fd47b5f033bb20973feda6362dac6089bd1863deaf3815d415c0083886080";
	
	strcpy(file.filename, "happyfish.pdf");
	file.size = 83886080L;
	strcpy(file.hashcode, file_hashcode);
	gettimeofday(&(file.timeval), NULL);
	cout<<"Init file info"<<endl;
	strcpy(block.hashcode, block_hashcode);
	block.size = 83886080L;
	strcpy(block.storagename, "storage01");
	cout<<"Init block info"<<endl;
	RedisAdaptor adaptor;
	RedisReply reply;
	adaptor.Connect();
	cout<<"connected"<<endl;
/*
	adaptor.Command(reply, "SET hello %s", "world");
	cout<<"SET hello: "<<reply.Str()<<endl;
	adaptor.Command(reply, "GET hello");
	cout<<"GET hello: "<<reply.Str()<<endl;
	adaptor.Command(reply, "SET data %b", &a, sizeof(struct A));
	cout<<"SET data: "<<reply.Str()<<endl;
	adaptor.Command(reply, "GET data");
	cout<<"GET data end"<<endl;
	p_a = (struct A *)reply.Str();
	cout<<p_a->buf<<" "<<p_a->length<<endl;
	adaptor.Command(reply, "SET data %b", &b, sizeof(struct B));
	//delete t_data;
	adaptor.Command(reply, "GET data");
	p_b = (struct B *)reply.Str();
	cout<<"GET data: "<<p_b->p<<" length: "<<p_b->length<<endl;
*/
	do
	{
		adaptor.Command(reply, "LPOP %s", file_hashcode);
		cout<<"delete a record"<<endl;
	}while(reply.Str()!=NULL);
	
	adaptor.Command(reply, "RPUSH %s %b", file_hashcode, &file, sizeof(struct fileInfo));
	adaptor.Command(reply, "RPUSH %s %b", file_hashcode, &block, sizeof(struct blockInfo));

	adaptor.Command(reply, "LRANGE %s 0 -1", file_hashcode);

	if(reply.Type() == REDIS_REPLY_ARRAY)
	{
		
		redisReply *r = (redisReply *)reply.Array_at(0);
		p_file = (struct fileInfo *)r->str;;
		cout<<"find file:"<<p_file->filename<<endl;
		for(int i = 1 ; i < reply.Array_size(); ++i)
		{
			redisReply *r = (redisReply *)reply.Array_at(i);
			p_block = (struct blockInfo *)r->str;
			cout<<"> block: "<<p_block->hashcode<<" found, in "<<p_block->storagename<<endl;
		}
	}

	adaptor.DisConnect();
	return 0;
}
