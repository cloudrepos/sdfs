/*
 * Storage.cpp
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#include "Storage.h"
#include "CToolKit.h"

namespace sdfs {

Storage::Storage(IRunnable *runner,const char *name) {
	m_runner = runner;
	m_name = (char *)name;
	m_storage = NULL;
	m_thread_threadpool = NULL;
	m_threadpool = NULL;
}

Storage::~Storage() {
	Stop();
}

int Storage::ReadConfiguration()
{
	strcpy(m_sConf.storage.strIp, LOCAL_HOST);
	strcpy(m_sConf.storage.strName, m_name);
	m_sConf.storage.nPort	= STORAGE_PORT;
	CToolKit::GetIpByName(m_sConf.storage.strName, m_sConf.storage.strIp, IP4_STR_LEN);
	m_storage = new Socket(&(m_sConf.storage));
	return 0;
}

int Storage::OpenListenSocket()
{
	return m_storage->CreateListenSocket();
}

int Storage::InitFileTable()
{
	//TODO:
	return 0;
}

int Storage::StartWorkers()
{
	m_threadpool = new ThreadPool(*m_runner);
	m_thread_threadpool = new Thread(m_threadpool);
	return m_thread_threadpool->Start();
}

int Storage::Start()
{
	ReadConfiguration();
	int fd = OpenListenSocket();
	InitFileTable();
	StartWorkers();
	return fd;
}

int Storage::Stop()
{
	m_thread_threadpool->Stop();
	return 0;
}

int Storage::AddClient(int fd)
{
	return m_threadpool->addTask(fd);
}

} /* namespace sdfs */
