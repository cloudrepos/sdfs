/*
 * StorageWorker.h
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#ifndef STORAGEWORKER_H_
#define STORAGEWORKER_H_

#include "IRunnable.h"

namespace sdfs {

class StorageWorker: public sdfs::IRunnable {
public:
	StorageWorker();
	virtual ~StorageWorker();

	void *Run(void *arg);

	void Stop();
};

} /* namespace sdfs */
#endif /* STORAGEWORKER_H_ */
