/*
 * Storage.h
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#ifndef STORAGE_H_
#define STORAGE_H_
#include "common_define.h"
#include "Socket.h"
#include "IRunnable.h"
#include "ThreadPool.h"

namespace sdfs {

typedef struct _StorageConfig
{
	ServerConfig	storage;
}StorageConfig;

class Storage {
public:
	Storage(IRunnable *runner,const char *name);
	virtual ~Storage();
private:
	int ReadConfiguration();

	int OpenListenSocket();

	int InitFileTable();

	int StartWorkers();
public:
	int Start();

	int Stop();

	int AddClient(int fd);
private:
	StorageConfig	m_sConf;
	Socket			*m_storage;
	IRunnable		*m_runner;
	ThreadPool		*m_threadpool;
	Thread			*m_thread_threadpool;
	//TODO:temporary variable
	char			*m_name;
};

} /* namespace sdfs */
#endif /* STORAGE_H_ */
