/*
 * StorageWorker.cpp
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#include "StorageWorker.h"
#include "common_define.h"
#include "Log.h"
#include "CToolKit.h"
#include "ThreadPool.h"
#include "Task.h"

namespace sdfs {

StorageWorker::StorageWorker() {
	// TODO Auto-generated constructor stub

}

StorageWorker::~StorageWorker() {
	// TODO Auto-generated destructor stub
}

void *StorageWorker::Run(void *arg)
{
	Task *task = (Task *)arg;
	int fd = task->GetDescriptor();
	int n = 0;
	char buf[LINE_MAX];
	Log::Debug("reading fd:%d ...", fd);
	if((n = read(fd, buf, LINE_MAX)) < 1)
	{
		return (void *)-1;
	}
	cout<<buf<<endl;
	strcpy(buf, "hello tracker!\r\n");
	write(fd, buf, strlen(buf)+1);

	return (void *)0;
}

void StorageWorker::Stop()
{

}

} /* namespace sdfs */
