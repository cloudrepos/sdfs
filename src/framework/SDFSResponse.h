/*
 * SDFSResponse.h
 *
 *  Created on: 2013年8月17日
 *      Author: sun
 */

#ifndef SDFSRESPONSE_H_
#define SDFSRESPONSE_H_

#include "Response.h"
#include "Task.h"


namespace sdfs {

struct ResponseHeader
{
	ver_t	ver;
	resp_statuscode_t	code;
	context_len_t	len;
	char	hashcode[256];
};

class SDFSResponse: public Response {
public:
	SDFSResponse(Task *task);
	virtual ~SDFSResponse();

	virtual void SetStatusCode(resp_statuscode_t code);

	virtual void SetContextLength(context_len_t length);

	virtual void SetHashCode(const char *hashcode);

	virtual int WriteContext(const char *buf, const int size);

	virtual int SendHeader();

	virtual void SetVer(unsigned char ver);

private:
	Task *m_task;
	struct ResponseHeader m_head;
};

} /* namespace sdfs */
#endif /* SDFSRESPONSE_H_ */
