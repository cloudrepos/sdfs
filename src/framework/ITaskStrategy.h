/*
 * ITaskStrategy.h
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#ifndef ITASKSTRATEGY_H_
#define ITASKSTRATEGY_H_
#include "Framework.h"
#include "ITaskCarrier.h"

namespace sdfs {

class ITaskStrategy {
public:
	virtual ~ITaskStrategy();

	virtual ITaskCarrier* GetCarrier(void* head) = 0;
};

} /* namespace sdfs */
#endif /* ITASKSTRATEGY_H_ */
