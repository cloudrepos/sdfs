#include "Framework.h"

namespace sdfs
{
inline const char *reqstr(req_method_t method)
{
	return Request_Method[(req_method_t)method].value.strname;
}

inline const char* respstr(resp_statuscode_t code)
{
	int i;
	for(i = 0; Response_Status[i].key != RESP_STATUS_UNKNOW; ++i)
	{
		if(Response_Status[i].key == code)
			break;
	}
	return Response_Status[i].value;
}

}
