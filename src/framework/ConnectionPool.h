/*
 * ConnectionPool.h
 *
 *  Created on: 2013年7月30日
 *      Author: sun
 */

#ifndef CONNECTIONPOOL_H_
#define CONNECTIONPOOL_H_

#include "LinkedList.h"
#include "common_define.h"
#include "Mutex.h"
#include "IConnector.h"

namespace sdfs {


class ConnectionPool {
public:
	ConnectionPool(IConnector* connector, int poolsize = CONNECTION_POOL_SIZE, bool isBlock = false);
	virtual ~ConnectionPool();

	IConnector *GetConnection();

	int ReturnConnection(IConnector *connector);

private:
	void BuildConnections();

	void CloseConnections();


private:
	LinkedList<IConnector*>	*m_connector;
	int					m_nSize;
	Mutex				m_mutex;
};

} /* namespace sdfs */
#endif /* CONNECTIONPOOL_H_ */
