/*
 * SDFSResponse.cpp
 *
 *  Created on: 2013年8月17日
 *      Author: sun
 */

#include "SDFSResponse.h"
#include "common_define.h"
#include "CToolKit.h"
#include "Framework.h"
#include "Task.h"

namespace sdfs {

SDFSResponse::SDFSResponse(Task *task) {
	m_task = task;
	memset(&m_head,0,sizeof(ResponseHeader));
}

SDFSResponse::~SDFSResponse() {
}

void SDFSResponse::SetVer(unsigned char ver)
{
	m_head.ver = ver;
}

void SDFSResponse::SetStatusCode(resp_statuscode_t code)
{
	m_head.code = code;
}

void SDFSResponse::SetContextLength(context_len_t length)
{
	m_head.len = length;
}

int SDFSResponse::WriteContext(const char *buf, const int size)
{
	return CToolKit::Writen(m_task->GetDescriptor(), buf, size);
}

int SDFSResponse::SendHeader()
{
	return CToolKit::Writen(m_task->GetDescriptor(), &m_head, sizeof(struct ResponseHeader));
}

void SDFSResponse::SetHashCode(const char *hashcode)
{
	memcpy(m_head.hashcode, hashcode, 256);
}

} /* namespace sdfs */
