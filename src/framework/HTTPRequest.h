/*
 * HTTPRequest.h
 *
 *  Created on: 2013年9月23日
 *      Author: sun
 */

#ifndef HTTPREQUEST_H_
#define HTTPREQUEST_H_

#include "Request.h"
#include "Socket.h"

namespace sdfs {

const char *http_version = "1.0";
const char *http_head_pattern = "%s %s HTTP/%s\r\nContent-Type: %s\r\n\r\n";
const char *http_put = "PUT";
const char *http_get = "GET";
const char *http_del = "DELETE";
const char *http_head = "HEAD";

class HTTPRequest :public Request{
private:
	HTTPRequest(Socket *socket,
			const char *ver,
			const char *resource,
			const char *method );
public:

	static HTTPRequest *GETReq(Socket *sock, const char *resource);

	static HTTPRequest *DELETEReq(Socket *sock, const char *resource);

	static HTTPRequest *PUTReq(Socket *sock, const char *resource);

	static HTTPRequest *HEADReq(Socket *sock, const char *resource);

	virtual ~HTTPRequest();

	unsigned char GetVersion();

	req_method_t GetMethod();

	context_len_t GetContextLength();

	int SendHeader();

	int SendContext(const char *buf, int size);

	int ReadContext(char *buf, int size);

	int ReadHeader(void *head, int size);
private:
	Socket *m_pSocket;
	const char *m_ver;
	const char *m_method;
	const char *m_resource;
	char *m_buf;
	int m_requestSize;
};

} /* namespace sdfs */
#endif /* HTTPREQUEST_H_ */

