/*
 * Request.h
 *
 *  Created on: 2013年8月16日
 *      Author: sun
 */

#ifndef REQUEST_H_
#define REQUEST_H_
#include "Framework.h"

namespace sdfs {

class Request {
public:
	Request();
	virtual ~Request();
public:
	virtual unsigned char GetVersion() = 0;

	virtual req_method_t GetMethod() = 0;

	virtual context_len_t GetContextLength() = 0;

	virtual int SendHeader() = 0;

	virtual int SendContext(const char *buf, int size) = 0;

	virtual int ReadContext(char *buf, int size) = 0;

	virtual int ReadHeader(void *head, int size) = 0;
};

} /* namespace sdfs */
#endif /* REQUEST_H_ */
