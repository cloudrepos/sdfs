/*
 * TaskQueue.h
 *
 *  Created on: 2013年7月22日
 *      Author: sun
 */

#ifndef TASKQUEUE_H_
#define TASKQUEUE_H_

#include <queue>
#include <vector>
#include "Mutex.h"
#include "Task.h"
#include "ThreadCondition.h"
#include "../common/Log.h"

namespace sdfs {

class TaskQueue {
private:
	TaskQueue(int maxsize)
	{
		m_nMaxSize = maxsize;
		m_queue = new std::queue<Task*>;
		m_waiting = true;
	}
public:

	static TaskQueue* GetInstance()
	{
		return (TaskQueue *)instance;
	}

	virtual ~TaskQueue()
	{
		while(!m_queue->empty())
		{
			delete m_queue->back();
			m_queue->pop();
		}
		delete m_queue;
	}

	void Add(Task* task)
	{
		m_cond.Lock();
		Log::Debug("add Task fd: %d", task->GetDescriptor());
		m_queue->push(task);
		m_cond.Unlock();
		m_cond.Signal();
	}
	/**
	 * GetTask will block, if no task in the queue, the method will
	 * be waked up by Add
	 */
	Task* GetTask()
	{
		Task *p;
		m_cond.Lock();
		while(m_queue->size()< 1 && m_waiting)
		{
			int err = m_cond.Wait();
			Log::Debug("waked...");
			if(err != 0)
			{
				Log::Error("File: "__FILE__", line: %d, " \
				"call pthread_join, errno: %d, info: %s",
				__LINE__, err, STRERROR(err));
				m_cond.Unlock();
				return NULL;
			}
		}
		if(!m_waiting)
		{
			m_cond.Unlock();
			return NULL;
		}
		Log::Debug("Got a task!");
		p = m_queue->front();
		m_queue->pop();
		m_cond.Unlock();
		return p;
	}

	int StopWaiting()
	{
		m_waiting = false;
		return m_cond.Broadcast();
	}

	bool IsEmpty()
	{
		bool rst;
		m_cond.Lock();
		rst =  m_queue->empty();
		m_cond.Unlock();
		return rst;
	}
	bool IsFull()
	{
		bool rst;
		m_cond.Lock();
		rst =  m_queue->size() == m_nMaxSize ? true : false;
		m_cond.Unlock();
		return rst;
	}

	int GetSize()
	{
		int rst;
		m_cond.Lock();
		rst =  m_queue->size();
		m_cond.Unlock();
		return rst;
	}

	void Destroy()
	{
		delete this;
	}
private:
	unsigned int		m_nMaxSize;
	std::queue<Task*>*	m_queue;
	CThreadCondition	m_cond;
	static const TaskQueue *instance;
	bool				m_waiting;
};

} /* namespace sdfs */
#endif /* TASKQUEUE_H_ */
