/*
 * HTTPRequest.cpp
 *
 *  Created on: 2013年9月23日
 *      Author: sun
 */

#include "HTTPRequest.h"
#include "common_define.h"

namespace sdfs {

HTTPRequest::HTTPRequest(Socket *socket,
		const char *ver,
		const char *resource,
		const char *method ) {
	m_pSocket = socket;
	m_ver = ver;
	m_method = method;
	m_buf = new char[LINE_MAX];
	m_requestSize = snprintf(m_buf, LINE_MAX, http_head_pattern, m_method, m_resource, m_ver);

}

HTTPRequest::~HTTPRequest() {
	delete []m_buf;
	m_buf = NULL;
}

HTTPRequest *HTTPRequest::GETReq(Socket *socket, const char *resource)
{
	return new HTTPRequest(socket, http_version, resource, http_get);
}

HTTPRequest *HTTPRequest::DELETEReq(Socket *socket, const char *resource)
{
	return new HTTPRequest(socket, http_version, resource, http_del);
}

HTTPRequest *HTTPRequest::PUTReq(Socket *socket, const char *resource)
{
	return new HTTPRequest(socket, http_version, resource, http_put);
}

HTTPRequest *HTTPRequest::HEADReq(Socket *socket, const char *resource)
{
	return new HTTPRequest(socket, http_version, resource, http_head);
}

int HTTPRequest::SendHeader()
{
	return m_pSocket->Write(m_buf, m_requestSize);
}

int HTTPRequest::SendContext(const char *buf, int size)
{
	return m_pSocket->Write(buf, size);
}

int HTTPRequest::ReadContext(char *buf, int size)
{
	return m_pSocket->Read(buf, size);
}

int HTTPRequest::ReadHeader(void *head, int size)
{
	return m_pSocket->Read(head, size);
}

} /* namespace sdfs */
