/*
 * Task.cpp
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#include "Task.h"
//#include "../common/CToolKit.h"
//#include "../common/Log.h"
//#include <errno.h>

namespace sdfs {

Task::Task(int fd) {
	uuid_generate(m_taskid);
	this->m_sockfd = fd;
}

Task::~Task() {
}
//
//int Task::BuildTask()
//{
//	int length = read(m_socket->GetDescriptor(), m_buf, REQUEST_HEAD_MAX_LEN);
//	if(length < 1)
//		return EINTR;
//	m_buf[length] = '\0';
//	char* p = m_buf;
//	while(*p != ' ' && *p!= '\0')
//	{
//		p++;
//	}
//	if(*p != ' ')
//		return ENOCMD;
//	*p = '\0';
//	p++;
//
//	int i = 0;
//	while(strcmp(m_buf, METHODS[i].name)!=0)
//	{
//		i++;
//	}
//	if(i < sizeof(METHODS))
//	{
//		m_type = METHODS[i].type;
//		m_nWeight = METHODS[i].weight;
//		m_arg = p;
//		return 0;
//	}
//	return ENOCMD;
//}
//
int Task::GetDescriptor()
{
	return m_sockfd;
}

uuid_t* Task::GetTaskId()
{
	return &this->m_taskid;
}
//
//TaskType Task::GetTaskType()
//{
//	return this->m_type;
//}
//
//char* Task::GetParamter()
//{
//	return this->m_arg;
//}
//
//unsigned int Task::GetWeight()
//{
//	return this->m_nWeight;
//}

Task *Task::CreateTask(int fd)
{
	return new Task(fd);
}

void Task::Destroy()
{
	delete this;
}

} /* namespace sdfs */
