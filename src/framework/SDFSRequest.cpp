/*
 * SDFSRequest.cpp
 *
 *  Created on: 2013年8月16日
 *      Author: sun
 */

#include "SDFSRequest.h"
#include "Log.h"
#include "common_define.h"


namespace sdfs {

SDFSRequest::SDFSRequest(Task* _task) {
	m_socket = Socket::BuildConnectedSocket(_task->m_sockfd,true);
	m_bDelete = true;
}

SDFSRequest::SDFSRequest(Socket *socket)
{
	m_socket = socket;
	m_bDelete = false;
}

SDFSRequest::~SDFSRequest() {
	if(m_bDelete)
		delete m_socket;
}

unsigned char SDFSRequest::GetVersion()
{
	return m_header.ver;
}

int SDFSRequest::ReadHeader()
{
	int err;

	err = m_socket->Read(&m_header, sizeof(ver_t));
	if(err < 0)
	{
		Log::Error("file: "__FILE__", line: %d, "
				"read header error, errno: %d, "
				"info: %s", __LINE__, errno, STRERROR(errno));
		return err;
	}
	return 0;
}

void *SDFSRequest::GetRequestHead()
{
	return (void *)&m_header;
}

req_method_t SDFSRequest::GetMethod()
{
	return m_header.method;
}

context_len_t SDFSRequest::GetContextLength()
{
	return m_header.contextlength;
}

int SDFSRequest::ReadContext(char *buf, int size)
{
	int err;
	err = m_socket->Read(buf, sizeof(ver_t));
	if(err < 0)
	{
		Log::Error("file: "__FILE__", line: %d, "
				"read header error, errno: %d, "
				"info: %s", __LINE__, errno, STRERROR(errno));
		return err;
	}
	return err;
}


} /* namespace sdfs */
