/*
 * Task.h
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#ifndef TASK_H_
#define TASK_H_
#include "../common/common_define.h"
#include <map>
#include <uuid/uuid.h>
#include "Socket.h"
#include "../common/global.h"

namespace sdfs {

class Task {
private:
	Task(int fd);
public:
	virtual ~Task();

	//int BuildTask();

	int	GetDescriptor();

	uuid_t*	GetTaskId();

	//TaskType GetTaskType();

	//unsigned int GetWeight();

	//char* GetParamter();

	void Destroy();

	static Task *CreateTask(int fd);

private:
	uuid_t		m_taskid;
	int			m_sockfd;
	//TaskType		m_type;
	//unsigned int	m_nWeight;
	//char			*m_buf;
	//char			*m_arg;
};

} /* namespace sdfs */
#endif /* TASK_H_ */
