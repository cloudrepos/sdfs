#ifndef _ICONNECTOR_H_
#define _ICONNECTOR_H_

namespace sdfs
{

class IConnector
{
public:
	virtual ~IConnector(){};

	virtual int Connect() = 0;

	virtual int DisConnect() = 0;

	virtual IConnector* Clone() = 0;
};

}

#endif
