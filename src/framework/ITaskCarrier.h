/*
 * ITaskCarrier.h
 *
 *  Created on: 2013年8月1日
 *      Author: sun
 */

#ifndef ITASKCARRIER_H_
#define ITASKCARRIER_H_
#include "Framework.h"
#include "Request.h"
#include "Response.h"

namespace sdfs {

class ITaskCarrier {

public:
	virtual ~ITaskCarrier();

	virtual bool isTasktype(void* head) = 0;

	virtual void	*Upload(Request* req, Response* resp) = 0;

	virtual void	*Download(Request* req, Response* resp) = 0;

	virtual void	*Delete(Request* req, Response* resp) = 0;

	virtual void	*Get(Request* req, Response* resp) = 0;
};

} /* namespace sdfs */
#endif /* ITASKCARRIER_H_ */
