#ifndef _FRAMEWORK_H_
#define _FRAMEWORK_H_

#include <stdint.h>
#include "common_define.h"
#include "Epoll.h"

namespace sdfs
{
typedef struct _ServerConfig{
	char	strIp[IP4_STR_LEN];
	int		nPort;
	char	strName[SRV_NAME_LEN];
}ServerConfig;

enum request_method
{
	_unknow_ = 0x00,
	_search_ = 0x01,
	_delete_= 0x02,
	_upload_= 0x03,
	_download_= 0x04,
	_report_= 0x05
};

typedef unsigned char ver_t;
typedef unsigned long context_len_t;
typedef unsigned short int resp_statuscode_t;
typedef request_method req_method_t;

const resp_statuscode_t RESP_STATUS_UNKNOW=0;
const resp_statuscode_t RESP_STATUS_OK=200;
const resp_statuscode_t RESP_STATUS_ERR	=500;
const resp_statuscode_t RESP_STATUS_NF=404;

template<typename K, typename V>
struct pair
{
	K key;
	V value;
};

typedef struct _MethodInfo
{
	uint8_t	weight;
	const char	*strname;
}MethodInfo;

const pair<req_method_t, MethodInfo> Request_Method[] = {
		{_unknow_,{ 0,"UNKNOW"}},
		{_search_,{ 1,"SEARCH"}},
		{_delete_,{ 1,"DELETE"}},
		{_upload_,{ 5,"UPLOAD"}},
		{_download_,{ 5,"DOWNLOAD"}},
		{_report_,{ 1, "REPORT"}}
};

const pair<resp_statuscode_t, const char*> Response_Status[]=
{
		{RESP_STATUS_OK, "OK"},
		{RESP_STATUS_ERR, "Error"},
		{RESP_STATUS_NF, "Not Found"},
		{RESP_STATUS_UNKNOW, "Unknow"}
};

extern Epoll g_client_epoll;


extern inline const char *reqstr(req_method_t method);
extern inline const char *respstr(resp_statuscode_t code);

}
#endif
