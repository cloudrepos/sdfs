/*
 * Response.h
 *
 *  Created on: 2013年8月16日
 *      Author: sun
 */

#ifndef RESPONSE_H_
#define RESPONSE_H_
#include "Framework.h"

namespace sdfs {

class Response {
public:
	Response();
	virtual ~Response();
public:
	virtual void SetVer(unsigned char version) = 0;

	virtual void SetStatusCode(resp_statuscode_t code) = 0;

	virtual void SetHashCode(const char *hashcode) = 0;

	virtual void SetContextLength(context_len_t length) = 0;

	virtual int WriteContext(const char *buf, const int size) = 0;

	virtual int SendHeader() = 0;

};

} /* namespace sdfs */
#endif /* RESPONSE_H_ */
