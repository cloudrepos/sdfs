/*
 * ThreadPool.h
 *
 *  Created on: 2013年7月22日
 *      Author: sun
 */

#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include "Thread.h"
#include "TaskQueue.h"
#include "Pipe.h"
#include "IRunnable.h"
#include "../common/Epoll.h"
#include <signal.h>
#include <map>
#include <sys/epoll.h>

namespace sdfs {

class ThreadPool : public IRunnable{
public:
	ThreadPool(IRunnable& runner, int initgroupsize = THREAD_POOL_MIN_SIZE, int groupsize = THREAD_POOL_MAX_SIZE, int capatity= THREAD_POOL_GROUP_CAPACITY);
	virtual ~ThreadPool();

	int addTask(int sockfd);

	void *Run(void *arg);

	void Stop();

	void RemoveFromEpoll(int fd);

	int TaskSize();

	void StartNewThreadGroup();

	void StopWorkingThreadGroup();

private:
	std::vector<Thread*>	**m_pThreads;
	std::vector<int>		*m_pfds;
	IRunnable				*m_runner;
	int		m_nCurSize;
	int		m_nMinSize;
	int		m_nMaxSize;
	int		m_nGroupCapacity;
	Epoll	*m_epoll;
	Pipe 	m_pipe;
	bool	m_bContinue;
};

} /* namespace sdfs */
#endif /* THREADPOOL_H_ */
