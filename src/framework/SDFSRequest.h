/*
 * SDFSRequest.h
 *
 *  Created on: 2013年8月16日
 *      Author: sun
 */

#ifndef SDFSREQUEST_H_
#define SDFSREQUEST_H_

#include "Request.h"
#include "Task.h"
#include "Framework.h"
#include "Socket.h"
#include <uuid/uuid.h>

namespace sdfs {

struct RequsetHeader
{
	ver_t	ver;
	req_method_t	method;
	context_len_t	contextlength;
	uuid_t taskid;
};

class SDFSRequest: public Request {
public:
	SDFSRequest(Task* task);
	SDFSRequest(Socket *socket);
	virtual ~SDFSRequest();

	virtual req_method_t GetMethod();

	virtual context_len_t GetContextLength();

	virtual void *GetRequestHead();

	virtual int SendHeader();

	virtual int ReadContext(char *buf, int size);

	virtual int SendContext(char *buf, int size);

	virtual int ReadHeader();

	virtual unsigned char GetVersion();

private:
	Socket *m_socket;
	struct RequsetHeader m_header;
	bool m_bDelete;
};

} /* namespace sdfs */
#endif /* SDFSREQUEST_H_ */
