#ifndef COMMON_DEFINE_H
#define COMMON_DEFINE_H

#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <vector>
#include <errno.h>
#include <sys/time.h>
#include <syslog.h>
#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "errno_addition.h"

#define MAX_PATH_SIZE	255
#ifndef LINE_MAX
#define LINE_MAX		256
#endif

#define PATH_MAX_LEN	256

#define EPOLL_MAX_SIZE			1024
#define EPOLL_TIME_WAIT		10
#define THREAD_POOL_MIN_SIZE		1
#define THREAD_POOL_MAX_SIZE		100
#define THREAD_POOL_WAITTIME		-1
#define THREAD_POOL_GROUP_CAPACITY	10
#define TASKQUEUE_MAX_SIZE	10240

#define STRERROR(no) (strerror(no) != NULL ? strerror(no) : "Unkown error")
//#define get_current_time() (g_schedule_flag ? g_current_time: time(NULL))
#define get_current_time() time(NULL)
#define offsetof(type, member) (size_t)&(((type *)0)->member)

//======================BIT SUPPORT============================//
#define PTR_LEN	int32_t

//============================TASK=============================//
#define REQUEST_HEAD_MAX_LEN	512

//=========================LOGGER==============================//
#define ACCESS_LOG_PATH "/tmp/logs/sdfs/access"
#define ERROR_LOG_PATH "/tmp/logs/sdfs/error"
#define LOG_BUF_SIZE 1024
#define ROTATE_SIZE 1024*1024L
#define USE_CACHE	1
#define LOG_LEVEL LOG_DEBUG

//=========================CONFIG==============================//
#define CONF_PATH	"/usr/local/sdfs/conf"
#define TRACKER_CONF_NAME	"tracker.conf"
#define STORAGE_CONF_NAME	"storage.conf"

//=========================NETWORK==============================//
#define IP4_STR_LEN 16
#define SRV_NAME_LEN 20
#define BACKED_SERVER_SIZE	1
#define LOCAL_HOST	"127.0.0.1"
#define TRACKER_FRONT_PORT	8500
#define TRACKER_BACK_PORT	8510
#define STORAGE_PORT		8505
#define LISTEN_QUEUE		1024

//==========================DEBUG===============================//
#ifndef DEBUG
#define DEBUG 1
#endif

//=========================THREAD==============================//
#define DEFAULT_STACK_SIZE 10*1024*1024
#define THREAD_COND_WAIT_SEC	10
#define THREAD_COND_WAIT_NSEC	0
typedef void *(*thread_func)(void *arg);

//======================CONNECTION POOL========================//
#define CONNECTION_POOL_SIZE 10

using namespace std;

#endif
