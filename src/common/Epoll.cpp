/*
 * Epoll.cpp
 *
 *  Created on: 2013年7月29日
 *      Author: sun
 */

#include "Epoll.h"
#include "../common/common_define.h"
#include "../common/Log.h"

namespace sdfs {

Epoll::Epoll() {
	if((m_epfd = epoll_create(EPOLL_MAX_SIZE))<0)
	{
		Log::Error("error:%s, epoll_create", STRERROR(m_epfd));
	}
}

Epoll::~Epoll() {
	// TODO Auto-generated destructor stub
}

int Epoll::AddEvent(int fd, uint32_t flag)
{
	epoll_event event;
	event.events =  flag;
	event.data.fd = fd;

	int result = epoll_ctl(m_epfd, EPOLL_CTL_ADD, fd, &event);
	if(result != 0)
	{
		Log::Error("error:%s, epoll_ctl", STRERROR(result));
		return 1;
	}
	return 0;
}

int Epoll::DeleteEvent(int fd)
{
	 return epoll_ctl(m_epfd, EPOLL_CTL_DEL, fd, NULL);
}

int Epoll::Wait(epoll_event *events, const int size, int waittime)
{
	return epoll_wait(m_epfd, events, size, waittime);
}

} /* namespace sdfs */
