/*
 * CToolKit.h
 *
 *  Created on: 2013年7月22日
 *      Author: sun
 */

#ifndef CTOOLKIT_H_
#define CTOOLKIT_H_

#include "common_define.h"

namespace sdfs {

class CToolKit {
public:
	CToolKit();
	virtual ~CToolKit();

	static int setNoblock(int fd);

	static int setBlock(int fd);

	static size_t Readn(int fd, void *buf, size_t num);

	static size_t Writen(int fd, const void *buf, size_t num);

	static char *GetIpByName(const char *name, char* buf, int size);
};

} /* namespace sdfs */
#endif /* CTOOLKIT_H_ */
