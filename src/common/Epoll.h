/*
 * Epoll.h
 *
 *  Created on: 2013年7月29日
 *      Author: sun
 */

#ifndef EPOLL_H_
#define EPOLL_H_

#include <sys/epoll.h>

namespace sdfs {

class Epoll {
public:
	Epoll();
	virtual ~Epoll();

	int AddEvent(int fd, uint32_t flag);

	int DeleteEvent(int fd);

	int Wait(epoll_event *events, const int size, int waittime = -1);

private:
	int		m_epfd;
};

} /* namespace sdfs */
#endif /* EPOLL_H_ */
