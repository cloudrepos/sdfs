/*
 * Log.cpp
 *
 *  Created on: 2013-7-16
 *      Author: sun
 */


#include <stdarg.h>
#include "Log.h"
#include "common_define.h"
#include "Logger.h"
#include "global.h"

namespace sdfs {

Logger g_access_logger(LogContextBuilder::getAccessLogger());
Logger g_error_logger(LogContextBuilder::getErrorLogger());

Log::Log() {

}


void Log::Debug(const char *format, ...)
{
#ifdef DEBUG
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_access_logger.log(LOG_DEBUG, text, len);
#endif
	return;
}

void Log::Info(const char *format, ...)
{
#if LOG_LEVEL >= LOG_INFO
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_access_logger.log(LOG_INFO, text, len);
#endif
}

void Log::Notice(const char *format, ...)
{
#if LOG_LEVEL >= LOG_NOTICE
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_access_logger.log(LOG_NOTICE, text, len);
#endif
}

void Log::Warning(const char *format, ...)
{
#if LOG_LEVEL >= LOG_WARNING
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_access_logger.log(LOG_WARNING, text, len);
#endif
}

void Log::Error(const char *format, ...)
{
#if LOG_LEVEL >= LOG_ERR
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_error_logger.log(LOG_ERR, text, len);
#endif
}

void Log::Crit(const char *format, ...)
{
#if LOG_LEVEL >= LOG_CRIT
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_error_logger.log(LOG_CRIT, text, len);
#endif
}

void Log::Emerge(const char *format, ...)
{
#if LOG_LEVEL >= LOG_EMERG
	char text[LINE_MAX];
	int len = 0;
	va_list ap;
	va_start(ap, format);
	len = vsnprintf(text, sizeof(text), format, ap);
	va_end(ap);
	g_error_logger.log(LOG_EMERG, text, len);
#endif
}

} /* namespace sdfs */
