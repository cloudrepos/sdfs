/*
 * Log.h
 *
 *  Created on: 2013-7-16
 *      Author: sun
 */

#ifndef LOG_H_
#define LOG_H_
#include "Logger.h"

namespace sdfs {

extern Logger g_access_logger;
extern Logger g_error_logger;

class Log {
private:
	Log();
public:
	static void Debug(const char *format, ...);
	static void Info(const char *format, ...);
	static void Notice(const char *format, ...);
	static void Warning(const char *format, ...);
	static void Error(const char *format, ...);
	static void Crit(const char *format, ...);
	static void Emerge(const char *format, ...);
};

} /* namespace sdfs */
#endif /* LOG_H_ */
