/*
 * File.h
 *
 *  Created on: 2013年8月27日
 *      Author: sun
 */

#ifndef FILE_H_
#define FILE_H_

namespace sdfs {

class File {
public:
	File();
	virtual ~File();



private:
	int m_fd;
};

} /* namespace sdfs */
#endif /* FILE_H_ */
