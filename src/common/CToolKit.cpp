/*
 * CToolKit.cpp
 *
 *  Created on: 2013年7月22日
 *      Author: sun
 */

#include "CToolKit.h"
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "Log.h"

namespace sdfs {

CToolKit::CToolKit() {
	// TODO Auto-generated constructor stub

}

CToolKit::~CToolKit() {
	// TODO Auto-generated destructor stub
}

int CToolKit::setNoblock(int fd)
{
	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFD, 0)|O_NONBLOCK) == -1)
	{
		return -1;
	}
	return 0;
}

int CToolKit::setBlock(int fd)
{
	if (fcntl(fd, F_SETFL, fcntl(fd, F_GETFD, 0)^~O_NONBLOCK) == -1)
	{
		return -1;
	}
	return 0;
}

char *CToolKit::GetIpByName(const char *name, char* buf, int size)
{
	struct hostent *phost;
	char	**pptr;
	if(strlen(name)<0)
		return NULL;
	phost = gethostbyname(name);
	if(phost == NULL)
	{
		Log::Warning("can not find host by name: %s", name);
		return NULL;
	}
	switch(phost->h_addrtype)
	{
	case AF_INET:
		 pptr=phost->h_addr_list;
		 for(;*pptr!=NULL;pptr++)
		 {
			 return (char *)inet_ntop(phost->h_addrtype, *pptr, buf, size);
		 }
		 break;
	default:
		Log::Warning("unknown address type");
		return NULL;
	}
	return NULL;
}

size_t CToolKit::Readn(int fd, void *buf, size_t num)
{
	ssize_t res;
	size_t n;
	char *ptr;

	n = num;
	ptr = (char *)buf;
	while (n > 0) {
		if ((res = read(fd, ptr, n)) == -1) {
			if (errno == EINTR)
				res = 0;
		else
			return (-1);
	     }
		else if (res == 0)
			break;
		ptr += res;
		n -= res;
	}
	return (num - n);

}

size_t CToolKit::Writen (int fd, const void *buf, size_t num)
{
	size_t res;
	size_t n;
	const char *ptr;

	n = num;
	ptr = (const char *)buf;
	while (n > 0) {
		 if ((res = write (fd, ptr, n)) <= 0) {
		  if (errno == EINTR)
		   res = 0;
		  else
		   return (-1);
		 }

		 ptr += res;
		 n -= res;
	}

	return (num);
}

} /* namespace sdfs */
