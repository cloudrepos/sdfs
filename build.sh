#!/bin/bash
. env.sh

if [ $# == 1 ];then
	if [ $1 == 'clean' ];then
		rm -f obj/*
		cd src;
		make clean;
	elif [ $1 == 'tracker' ];then
		cd src;
		make tracker;
		make -f makeobj;
	elif [ $1 == 'storage' ];then
		cd src;
		make storage;
		make -f makeobj;
	else
		echo "$0 [clean][tracker][storage]"
	fi
else
	echo "build.sh [clean][tracker][storage]"
fi
